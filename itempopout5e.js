class ItemPopoutDialog extends Application {
    constructor(itemData, options) {
        super(options);
        
        // Store a reference to the passed Actor
        this.item = itemData.data;
        this.data = itemData.templateData.data;
        this.actor = itemData.actor;
    }
  
  
    /**
     * Give the window a dynamic title which depends on the referenced Actor
     */
    get title() {
        return `${this.actor.name} - ${this.item.name}`;
    }
  
    /**
     * Get data to render popout
     */
    getData() {
        return this;
    }
    
    /**
     * Activate HTML Listeners
     */
    activateListeners(html) {

        // Chat card actions
        html.on('click', '.card-buttons button', ev => {
            ev.preventDefault();            

            // Extract card data
            let button = $(ev.currentTarget);
    
            // Extract action data
            let action = button.attr("data-action"),
                card = button.parents('.chat-card'),
                actor = game.actors.get(card.attr('data-actor-id')),
                itemId = Number(card.attr("data-item-id"));
    
            // Get the item
            if ( !actor ) return;
            let itemData = actor.items.find(i => i.id === itemId);
            if ( !itemData ) return;
            let item = new Item5e(itemData, actor);
    
            // Weapon attack
            if ( action === "weaponAttack" ) item.rollWeaponAttack(ev);
            else if ( action === "weaponDamage" ) item.rollWeaponDamage(ev);
            else if ( action === "weaponDamage2" ) item.rollWeaponDamage(ev, true);
    
            // Spell actions
            else if ( action === "spellAttack" ) item.rollSpellAttack(ev);
            else if ( action === "spellDamage" ) item.rollSpellDamage(ev);
    
            // Feat actions
            else if ( action === "featAttack" ) item.rollFeatAttack(ev);
            else if ( action === "featDamage" ) item.rollFeatDamage(ev);
    
            // Consumable usage
            else if ( action === "consume" ) item.rollConsumable(ev);
    
            // Tool usage
            else if ( action === "toolCheck" ) item.rollToolCheck(ev);
        });


    }
  }

class PopoutItem5e extends CONFIG.Item.entityClass {

    async roll() {

        // Calculate attributes to render.
        const template = `public/systems/dnd5e/templates/chat/${this.data.type}-card.html`;
        const templateData = {
            actor: this.actor,
            item: this.data,
            data: this.getChatData()
        };

        // ******** Start customisation for itempopout5e ********
        this.templateData = templateData;
        // Push to Chat (holding down shift or control will push the item-card to chat instead of a popout)
        if ( [KEYS.SHIFT, KEYS.CTRL].some(k => keyboard.isDown(k)) ) {
        // ******** End customisation for itempopout5e ********

            // Basic chat message data
            const chatData = {
                user: game.user._id,
                alias: this.actor.name,
            };
        
            // Toggle default roll mode
            let rollMode = game.settings.get("core", "rollMode");
            if ( ["gmroll", "blindroll"].includes(rollMode) ) chatData["whisper"] = ChatMessage.getWhisperIDs("GM");
            if ( rollMode === "blindroll" ) chatData["blind"] = true;
        
            // Render the template
            chatData["content"] = await renderTemplate(template, templateData);
        
            // Create the chat message
            return ChatMessage.create(chatData, {displaySheet: false});

        } 

        // ******** Customised for itempopout5e ********
        new ItemPopoutDialog(this, {width: 350, top: 80, left: window.innerWidth - 910, template: template}).render(true);

    }
    
}

// overwriting the default Item5e sheet
CONFIG.Item.entityClass = PopoutItem5e;