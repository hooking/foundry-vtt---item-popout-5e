# Item Popout (5e)

This module changes the behaviour of an item click. Instead of creating a chat-message, a new application dialog (popout) is created.
- Holding **shift** or **ctrl** keys will supress the popout and display the item-card in chat as per the default behaviour.

![example](preview.gif)

### Rational

My players have given me feedback on the default 5e system that they don't like spamming chat when they are planning their turn and also their planned action can get lots in chat from other peoples spam.

### Current Issues:
- Currently extending the default item5e class and overwriting the roll() method. Would prefer to use a hook and leave the 5e system alone so to avoid breaking changes in the future.
- Would like to add a button to show the item in chat (rather than holding the **shift** or **ctrl** keys.

### Installation Instructions

To install a module, follow these instructions:

1. [Download the zip file](https://gitlab.com/hooking/foundry-vtt---item-popout-5e/raw/master/itempopout5e.zip) included in the module directory. If one is not provided, the module is still in early development.
2. Extract the included folder to `public/modules` in your Foundry Virtual Tabletop installation folder.
3. Restart Foundry Virtual Tabletop.  

### Feedback

If you have any suggestions or feedback, please contact me on discord (hooking#0492)

Shoutout to Felix#6196 for the BetterNPCSheet module I used as an example.